const express = require("express");
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 29011;
let redis = require('redis');

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', (socket) => {
    console.log('a user connected');

    socket.on('auth', function (uid, callback) {
        let redisClient = redis.createClient();
        console.log(this.id);
        console.log(uid);
        redisClient.subscribe('notification');
        redisClient.on("message", function (channel, data) {
            console.log(channel);
            let arData = JSON.parse(data);
            let uid = arData.uid;
            console.log(uid);
            let phone = arData.phone;
            console.log(phone);
            socket.emit('notification', arData);
        });
    });
    socket.on('close', function (uid) {
        let arData = {
            uid: uid,
            close: 1
        };
        socket.broadcast.emit('notification', arData);
    });
    socket.on('disconnect', function () {
        console.log('user disconnected');
    });
});

http.listen(port, function () {
    console.log(`listening on port ${port}!`);
});
